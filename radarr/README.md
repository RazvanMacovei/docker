```
docker run -d 
	--name=radarr \
    --restart always \
    --network host   \
    -e TZ=Europe/Bucharest  \
	-p 7878:7878 \
	-v radarr-data:/config \
	-v /mnt/movies:/movies \
	--restart unless-stopped \   
linuxserver/radarr
```
