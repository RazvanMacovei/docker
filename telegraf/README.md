```
docker run -d \
	--name=telegraf \
    --net host \
	--restart always \
    -v /proc:/host/proc:ro \
	-v /mnt:/mnt \
	-v /var/run/docker.sock:/var/run/docker.sock \
    -v /opt/docker/telegraf/telegraf.conf:/etc/telegraf/telegraf.conf:ro \
telegraf
```
