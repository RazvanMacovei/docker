```
docker run -d 
	--name=jackett \
    --restart always \
    -e TZ=Europe/Bucharest  \
    -p 9117:9117 \
    -v jackett-data:/config \
linuxserver/jackett
```
