```
docker run -d 
	--name=sonarr \
    --restart always \
    --network host   \
    -e TZ=Europe/Bucharest  \
	-p 8989:8989 \
	-v sonarr-data:/config \
	-v /mnt/series:/tv \
	--restart unless-stopped \   
linuxserver/sonarr
```
