**1. Add ' vm.max_map_count=262144 ' in /etc/sysctl.conf and run 'sysctl -w vm.max_map_count=262144 '**

**2. Add in /etc/security/limits.conf**

elasticsearch soft memlock unlimited 

elasticsearch hard memlock unlimited



**3. Run**

 docker run -d \
 --restart always \
 --name elasticsearch \
 --ulimit memlock=-1:-1 \
 -v /etc/elastichsearch/config/elasticsearch.yml:/usr/share/elasticsearch/config/elasticsearch.yml \
 -v /mnt/live/esdata-storage/:/usr/share/elasticsearch/data \
 -e "ES_JAVA_OPTS=-Xms16G -Xmx16G" \
 -p 9200:9200 -p 9300:9300 \ 
 --network elasticsearch-backend \
 elasticsearch:7.4.0

**4. Curator**

curator_cli --host <elasticsearch_endpoint> delete_indices --filter_list '{"filtertype":"age","source":"name","timestring":"%Y.%m.%d","unit":"days","unit_count":60,"direction":"older"}'