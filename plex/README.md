```
docker run -d \
	--name=plex \
    --restart always \
	--network host \
	-e VERSION=docker \
	-e TZ=Europe/Bucharest \
	-v plex-data:/config \   
	-v /mnt/series:/series \   
	-v /mnt/movies:/movies  \ 
linuxserver/plex
```
