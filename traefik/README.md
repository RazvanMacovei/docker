```
docker run -d \
  --name=traefik \
  --restart always \
  --network host \
  -v /opt/docker/traefik/traefik.toml:/etc/traefik/traefik.toml \
  -v /opt/docker/traefik/configs:/etc/traefik/configs \
traefik:v1.7.9
```
