#!/bin/bash


cd /mnt/volume-backup
rm *.tar.gz

volume_list=$(ls /var/lib/docker/volumes | grep "data" | grep -v "metadata.db")

for volume in $volume_list
do
	bash /opt/docker/00_scripts/backup-docker-volume.sh $volume /mnt/volume-backup
done

cd /home/razvanm/mygoogledrive/Backups/Home@Server
rm *.tar.gz
rsync -ah --progress /mnt/volume-backup/*.tar.gz /home/razvanm/mygoogledrive/Backups/Home@Server/

