```
docker run -d \
  --name=qbittorrent \
  --restart always \
  -e TZ=Europe/Bucharest  \
  -e WEBUI_PORT=8080 \
  -p 6881:6881 \
  -p 6881:6881/udp \
  -p 8070:8080 \
  -v qbittorrent-data:/config \
  -v /mnt:/downloads \
linuxserver/qbittorrent
```
